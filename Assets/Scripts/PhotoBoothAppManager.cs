﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotoBoothAppManager : MonoBehaviour
{
    PhotoObjectsManager photoObjectsManager;
    CameraZoomController cameraController;
    CameraPhotoHandler cameraPhotoHandler;

    Button prevObjectButton;
    Button nextObjectButton;
    Button takePhotoButton;
    Slider cameraZoomSlider;

    private void Awake()
    {
        photoObjectsManager = FindObjectOfType<PhotoObjectsManager>();
        cameraController = Camera.main.GetComponent<CameraZoomController>();
        cameraPhotoHandler = Camera.main.GetComponent<CameraPhotoHandler>();

        prevObjectButton = GetComponentsInChildren<Button>()[0];
        nextObjectButton = GetComponentsInChildren<Button>()[1];
        takePhotoButton = GetComponentsInChildren<Button>()[2];
        cameraZoomSlider = GetComponentsInChildren<Slider>()[0];

        prevObjectButton.onClick.AddListener(delegate { photoObjectsManager.ActivatePreviousPhotoObject(); });
        nextObjectButton.onClick.AddListener(delegate { photoObjectsManager.ActivateNextPhotoObject(); });
        takePhotoButton.onClick.AddListener(delegate { cameraPhotoHandler.SavePhoto(); });
        cameraZoomSlider.onValueChanged.AddListener(cameraController.SetCameraZoom);

    }
}
