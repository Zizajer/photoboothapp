﻿using UnityEngine;
using System.Collections;

public class CameraZoomController : MonoBehaviour
{
    [SerializeField]
    float minCameraZoomZAxisPosition;
    [SerializeField]
    float maxCameraZoomZAxisPosiiton;

    Vector3 minCameraZoomPosition;
    Vector3 maxCameraZoomPosition;

    private void Awake()
    {
        minCameraZoomPosition = transform.position;
        maxCameraZoomPosition = transform.position;
    }

    public void SetCameraZoom(float sliderZoomValue)
    {
        minCameraZoomPosition.z = minCameraZoomZAxisPosition;
        maxCameraZoomPosition.z = maxCameraZoomZAxisPosiiton;
        transform.position = Vector3.Lerp(minCameraZoomPosition, maxCameraZoomPosition, 1 - sliderZoomValue);
    }
}
