﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotoObject : MonoBehaviour
{
    public void SetActive(bool value)
    {
        gameObject.SetActive(value);
    }

    void OnMouseDrag()
    {
        float rotationX = Input.GetAxis("Mouse X") * PhotoObjectsManager.Instance.PhotoObjectRotationSpeed * Mathf.Deg2Rad;
        float rotationY = Input.GetAxis("Mouse Y") * PhotoObjectsManager.Instance.PhotoObjectRotationSpeed * Mathf.Deg2Rad;

        transform.Rotate(Vector3.up, -rotationX);
        transform.Rotate(Vector3.right, rotationY);
    }
}
