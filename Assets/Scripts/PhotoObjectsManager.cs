﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class PhotoObjectsManager : MonoBehaviour
{
    public static PhotoObjectsManager Instance;

    [SerializeField]
    string pathToModelsFolder;

    [SerializeField]
    Vector3 defaultBoxColliderSize = new Vector3(3,3,3);
    [SerializeField]
    float photoObjectRotationSpeed = 20;

    [SerializeField]
    List<PhotoObject> createdPhotoObjects;

    int currentActiveObjectIndex;

    public float PhotoObjectRotationSpeed { get { return photoObjectRotationSpeed; } }

    private void Awake()
    {
        Instance = this;

        string[] files = Directory.GetFiles(pathToModelsFolder);

        foreach (string fileName in files)
        {
            string path = pathToModelsFolder + "/" + Path.GetFileName(fileName);

            var objectToCreate = AssetDatabase.LoadAssetAtPath(path, typeof(Object));

            if (objectToCreate != null)
            {
                PhotoObject modelToAdd = ((GameObject)Instantiate(objectToCreate)).AddComponent<PhotoObject>();
                modelToAdd.transform.parent = transform;
                modelToAdd.gameObject.AddComponent<BoxCollider>().size = defaultBoxColliderSize;
                modelToAdd.SetActive(false);
                createdPhotoObjects.Add(modelToAdd);
            }
        }

        currentActiveObjectIndex = 0;
        createdPhotoObjects[0].SetActive(true);
    }

    public void ActivateNextPhotoObject()
    {
        if (currentActiveObjectIndex + 1 < createdPhotoObjects.Count)
        {
            createdPhotoObjects[currentActiveObjectIndex].SetActive(false);
            currentActiveObjectIndex++;
            createdPhotoObjects[currentActiveObjectIndex].SetActive(true);
        }
    }
    public void ActivatePreviousPhotoObject()
    {
        if (currentActiveObjectIndex - 1 > -1)
        {
            createdPhotoObjects[currentActiveObjectIndex].SetActive(false);
            currentActiveObjectIndex--;
            createdPhotoObjects[currentActiveObjectIndex].SetActive(true);
        }
    }
}
