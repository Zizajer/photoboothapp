﻿using UnityEngine;
using System.IO;
using System;

public class CameraPhotoHandler : MonoBehaviour
{
    [SerializeField]
    string pathToPhotoFolder;

    Camera myCamera;
    bool takeAPhoto;

    void Awake()
    {
        myCamera = GetComponent<Camera>();
    }

    public void SavePhoto()
    {
        myCamera.targetTexture = RenderTexture.GetTemporary(Screen.width, Screen.height, 16);
        takeAPhoto = true;
    }

    private void OnPostRender()
    {
        if (takeAPhoto)
        {
            takeAPhoto = false;
            RenderTexture renderTexture = myCamera.targetTexture;

            Texture2D renderResult = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, false);
            Rect rect = new Rect(0, 0, renderTexture.width, renderTexture.height);
            renderResult.ReadPixels(rect, 0, 0);

            byte[] byteArray = renderResult.EncodeToPNG();
            string fileName = string.Format("{0:yyyy-MM-dd_HH-mm-ss-fff}", DateTime.Now) + ".png";
            File.WriteAllBytes(pathToPhotoFolder + "/" + fileName, byteArray);
            Debug.Log("Saved " + fileName);

            RenderTexture.ReleaseTemporary(renderTexture);
            myCamera.targetTexture = null;
        }
    }

}
